package com.mycompany.logica;

import com.mycompany.modelos.Administrador;
import com.mycompany.modelos.Cita;
import com.mycompany.modelos.Medicamento;
import com.mycompany.modelos.Medico;
import com.mycompany.modelos.Paciente;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class p {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, SQLException {
        
        //Realizando menu por consola, el cuál se implementa todas y cada una de las clases.
        
        Scanner lector = new Scanner(System.in);
        
        boolean continuar = true;
        
        while(continuar){
            
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("++++++++++++++++++ CONSULTORIO ONLINE ++++++++++++++++++");
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("+++                                                  +++");
            System.out.println("+++                                                  +++");
            System.out.println("+++              SELECCIONE UNA OPCIÓN               +++");
            System.out.println("+++                                                  +++");
            System.out.println("+++                                                  +++");
            System.out.println("+++  1. INGRESAR AL SISTEMA.                         +++");
            System.out.println("+++  2. REGISTRARSE.                                 +++");
            System.out.println("+++  3. SALIR.                                       +++");
            System.out.println("+++                                                  +++");
            System.out.println("+++  Digite opción \t"); 
            int opcionmenu = lector.nextInt();
            
            boolean menuusuarios = true;
            while(menuusuarios){
                switch(opcionmenu){
                    case 1:
                        System.out.println("\n\n\n");
                        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        System.out.println("++++++++++++++++++ MENU PRINCIPAL     ++++++++++++++++++");
                        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                        System.out.println("+++                                                  +++");
                        System.out.println("+++       SELECCIONE TIPO DE USUARIO                 +++");
                        System.out.println("+++  1. PACIENTE.                                    +++");
                        System.out.println("+++  2. MEDICO.                                      +++");
                        System.out.println("+++  3. ADMINISTRADOR                                +++");
                        System.out.println("+++  4. REGRESAR AL MENU PRINCIPAL.                  +++");
                        System.out.println("+++  5. SALIR.                                       +++");
                        System.out.println("+++                                                  +++");
                        int opcionusuario = lector.nextInt();
                        String cedula = "0";
                        boolean loginpaciente = true;
                        while(loginpaciente){
                            switch(opcionusuario){
                                case 1:
                                    System.out.println("\n\n\n");
                                    
                                    lector.nextLine();
                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    System.out.println("++++++++++++++  LOGIN PACIENTE   ++++++++++++++");
                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    System.out.println("+++                                                  +++");
                                    System.out.println("+++          INGRESE SUS CREDENCIALES                +++");
                                    System.out.println("+++                                                  +++");
                                    System.out.println("+++  CEDULA:");
                                    cedula = lector.nextLine();
                                    //System.out.println("+++  USUARIO:");
                                    //user = lector.nextLine();
                                    //System.out.println("+++  CONTRASENIA:");
                                    //pass = lector.nextLine();                                    
                                    
                                    Paciente p = new Paciente();
                                    boolean mp = true;
                                    while(mp){
                                        if(p.consultarPaciente(cedula)){
                                            lector.nextLine();
                                            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                            System.out.println("++++++++++++++  MENU PACIENTE             ++++++++++++++");
                                            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                            System.out.println("+++                                                  +++");
                                            System.out.println("+++          ¿QUE DESEA REALIZAR?                    +++");
                                            System.out.println("+++  1. REGISTRAR CITA.                              +++");
                                            System.out.println("+++  2. CONSULTAR CITA.                              +++");
                                            System.out.println("+++  3. ACTUALIZAR DATOS PERSONALES.                 +++");
                                            System.out.println("+++  4. SALIR.                                       +++");
                                            System.out.println("+++");
                                            int op = lector.nextInt();
                                            Cita c = new Cita();
                                            
                                            //if(op )
                                        
                                            switch(op){
                                                case 1:
                                                    //REGISTRAR CITAS.
                                                    System.out.println("\n\n\n");
                                                    lector.nextLine();                                                    
                                                    System.out.println("INGRESE EL TIPO DE CITA:");
                                                    c.setTipoCita(lector.nextLine());
                                                    lector.nextLine();                                                        
                                                    c.setFechaRegistro("08/10/2021");
                                                    System.out.println("INGRESE LA FECHA DE LA CITA:");
                                                    c.setFechaCita(lector.nextLine());
                                                    c.setPacienteCedula(cedula);
                                                    c.guardarCita();
                                                    break;
                                                    
                                                case 2:
                                                    //CONSULTAR CITAS
                                                    if(c.consultarCita(cedula)){
                                                        System.out.println("Hola " + p.getNombre() + " estas son tus citas: ");
                                                        
                                                        System.out.println("ID: " + c.getId());
                                                        System.out.println("TIPO CITA: " + c.getTipoCita());
                                                        System.out.println("FECHA REGISTRO: " + c.getFechaRegistro());
                                                        System.out.println("FECHA CITA: " + c.getFechaCita());
                                                        System.out.println("CEDULA PACIENTE: " +c.getPacienteCedula());
                                                        break;
                                                    }
                                                    else{
                                                        System.out.println("EL PACIENTE NO TIENE CITAS.");
                                                        break;
                                                    }
                                                    
                                                case 3:
                                                    //ACTUALIZAR LOS DATOS PERSONALES DE LA PERSONA
                                                    System.out.println("\n\n\n");
                                                    lector.nextLine();
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  ACTUALIZAR DATOS          ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("+++                                                  +++");
                                                    System.out.println("+++       ¿DIGITE LOS DATOS A ACTUALIZAR?            +++");
                                                    
                                                    System.out.println("SI NO DESEA MODIFICAR NINGUN DATO SOLO PRESIONE ENTER");
                                                    p.setCedula(cedula);
                                                    lector.nextLine();
                                                    System.out.println("SU NOMBRE: " + p.getNombre());
                                                    String nombrep = lector.nextLine();
                                                    if(nombrep != ""){
                                                        p.setNombre(nombrep);
                                                    }
                                                    lector.nextLine();
                                                    System.out.println("SU APELLIDO: " + p.getApellido());
                                                    String apellido = lector.nextLine();
                                                    if(apellido != ""){
                                                        p.setApellido(apellido);
                                                    }
                                                    System.out.println("SU EDAD: " + p.getEdad());
                                                    String edad = lector.nextLine();
                                                    if(edad != ""){
                                                        int ed = Integer.parseInt(edad);
                                                        p.setEdad(ed);
                                                    }
                                                    System.out.println("SU TELEFONO: " + p.getTelefono());
                                                    String telefono = lector.nextLine();
                                                    if(telefono != ""){
                                                        p.setTelefono(telefono);
                                                    }
                                                    System.out.println("SU CORREO: " + p.getCorreo());
                                                    String correo = lector.nextLine();
                                                    if(correo != ""){
                                                        p.setCorreo(correo);
                                                    }
                                                    System.out.println("SU DIRECCION: " + p.getDireccion());
                                                    String direccion = lector.nextLine();
                                                    if(direccion != ""){
                                                        p.setDireccion(direccion);
                                                    }
                                                    System.out.println("SU USUARIO: " + p.getUsuario());
                                                    String usuario = lector.nextLine();
                                                    if(usuario != ""){
                                                        p.setUsuario(usuario);
                                                    }
                                                    System.out.println("SU CONTRASEÑA: " + p.getContrasenia());
                                                    String contrasenia = lector.nextLine();
                                                    if(contrasenia != ""){
                                                        p.setContrasenia(contrasenia);
                                                    }
                                                    
                                                    p.actualizarPaciente(cedula);
                                                    System.out.println("PACIENTE REGISTRADO SATISFACTORIAMENTE");
                                                    
                                                    break;
                                                case 4:
                                                    //SALIR DEL MENU USUARIO
                                                    mp = false;
                                                    loginpaciente = false;
                                                    break;
                                                    
                                                default: 
                                                    System.out.println("OPCIÓN QUE HA INGRESADO ES INCORRECTA");
                                                    break;
                                            }
                                        }
                                        else if(cedula == ""){
                                            System.out.println("LAS CREDENCIALES NO PUEDEN ESTAR VACIAS."); 
                                        }
                                        else{
                                            System.out.println("CREDENCIALES INCORRECTAS.");
                                        }
                                    }
                                    
                                                                           
                                    break;

                                case 2:
                                    //OPCIONES DEL MÉDICO
                                    lector.nextLine();
                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    System.out.println("++++++++++++++  LOGIN MÉDICO              ++++++++++++++");
                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    System.out.println("+++                                                  +++");
                                    System.out.println("+++          INGRESE SUS CREDENCIALES                +++");
                                    System.out.println("+++                                                  +++");
                                    System.out.println("+++  CEDULA:");
                                    String cedulamedico = lector.nextLine();
                                    
                                    Medico medico = new Medico();
                                    boolean mm = true;
                                    while(mm){
                                        if(medico.consultarMedico(cedulamedico)){
                                            System.out.println("\n\n\n");
                                            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                            System.out.println("++++++++++++++  MENU MEDICO             ++++++++++++++");
                                            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                            System.out.println("+++                                                  +++");
                                            System.out.println("+++          ¿QUE DESEA REALIZAR?                    +++");
                                            System.out.println("+++  1. AGENDAR MEDICAMENTOS.                        +++");
                                            System.out.println("+++  2. SALIR.                                       +++");
                                            System.out.println("+++");
                                            int opp = lector.nextInt();
                                            
                                            switch(opp){
                                                case 1:
                                                    //Agendar medicamentos.
                                                    Medicamento medi = new Medicamento();
                                                    System.out.println("DEBERÍA AGENDAR MEDICAMENTOS PERO NO ESTÁ CREADA LA TABLA DE AGENDAS DE MEDICAMENTOS.");
                                                    break;
                                                    
                                                case 2:
                                                    //SALIR DEL MENU MEDICO
                                                    System.out.println("SALIENDO DEL MENÚ MÉDICO.");
                                                    mm = false;
                                                    break;
                                                 
                                                default: 
                                                    System.out.println("OPCIÓN QUE HA INGRESADO ES INCORRECTA");
                                                    break;
                                            }
                                        }
                                        else{
                                            System.out.println("LA CEDULA INGRESADA NO SE ENCUENTRA REGISTRADA.");
                                        }
                                        break;
                                    }
                                case 3: 
                                    //OPCIONES DEL ADMINISTRADOR
                                    lector.nextLine();
                                    System.out.println("\n\n\n");
                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    System.out.println("++++++++++++++  LOGIN ADMINISTRADOR       ++++++++++++++");
                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                    System.out.println("+++                                                  +++");
                                    System.out.println("+++          INGRESE SUS CREDENCIALES                +++");
                                    System.out.println("+++                                                  +++");
                                    System.out.println("+++  CEDULA:");
                                    String cedulaadmin = lector.nextLine();
                                    Administrador admin = new Administrador();
                                    boolean ma = true;
                                    while(ma){
                                        if(admin.consultarAdministrador(cedulaadmin)){
                                            System.out.println("\n\n\n");
                                            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                            System.out.println("++++++++++++++  MENU ADMINISTRADOR        ++++++++++++++");
                                            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                            System.out.println("+++                                                  +++");
                                            System.out.println("+++          ¿QUE DESEA REALIZAR?                    +++");
                                            System.out.println("+++  1. REGISTRAR MEDICAMENTOS.                      +++");
                                            System.out.println("+++  2. ACTUALIZAR MEDICAMENTOS.                     +++");
                                            System.out.println("+++  3. CONSULTAR MEDICAMENTOS.                     +++");
                                            System.out.println("+++  4. REGISTRAR CITAS.                             +++");
                                            System.out.println("+++  5. CONSULTAR CITAS.                             +++");
                                            System.out.println("+++  6. ACTUALIZAR CITAS.                            +++");
                                            System.out.println("+++  7. REGISTRAR MEDICOS.                           +++");
                                            System.out.println("+++  8. CONSULTAR MEDICOS.                           +++");
                                            System.out.println("+++  9. ACTUALIZAR MEDICOS.                          +++");
                                            System.out.println("+++  10. REGISTRAR PACIENTES.                         +++");
                                            System.out.println("+++  11. CONSULTAR PACIENTES.                        +++");
                                            System.out.println("+++  12. ACTUALIZAR PACIENTES.                       +++");
                                            System.out.println("+++  13. SALIR.                                      +++");
                                            System.out.println("+++");
                                            int opa = lector.nextInt();
                                            Medicamento medicamento = new Medicamento();
                                        
                                            switch(opa){
                                                case 1:
                                                    lector.nextLine();
                                                    //REGISTRAR MEDICAMENTOS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  REGISTRAR MEDICAMENTO.    ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    
                                                                                                        
                                                    
                                                    System.out.println("SU NOMBRE: ");
                                                    medicamento.setNombre(lector.nextLine());
                                                    
                                                    System.out.println("SU PRESENTACION: ");
                                                    medicamento.setPresentacion(lector.nextLine());
                                                    
                                                    
                                                    System.out.println("SU DESCRIPCION: ");
                                                    medicamento.setDescripcion(lector.nextLine());
                                                    
                                                    
                                                    System.out.println("SU FABRICANTE: ");
                                                    medicamento.setFabricante(lector.nextLine());
                                                    
                                                    medicamento.guardarMedicamento();
                                                    System.out.println("MEDICAMENTO GUARDADO CORRECTAMENTE.");
                                                    break;

                                                case 2:
                                                    //ACTUALIZAR MEDICAMENTO
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  ACTUALIZAR MEDICAMENTO.   ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    
                                                    System.out.println("INGRESE EL ID DEL MEDICAMENTO A ACTUALIZAR: ");
                                                    int id = lector.nextInt();
                                                    if(medicamento.consultarMedicamento(id)){
                                                        System.out.println("SI NO DESEA MODIFICAR NINGUN DATO SOLO PRESIONE ENTER");
                                                        lector.nextLine();

                                                        System.out.println("SU NOMBRE: " + medicamento.getNombre());
                                                        String nombre = lector.nextLine();
                                                        if(nombre != ""){
                                                            medicamento.setNombre(nombre);
                                                        }

                                                        System.out.println("SU PRESENTACION: " + medicamento.getPresentacion());
                                                        String presentacion = lector.nextLine();
                                                        if(presentacion != ""){
                                                            medicamento.setPresentacion(presentacion);
                                                        }

                                                        System.out.println("SU DESCRIPCION: " + medicamento.getDescripcion());
                                                        String descripcion = lector.nextLine();
                                                        if(descripcion != ""){
                                                            medicamento.setDescripcion(descripcion);
                                                        }

                                                        System.out.println("SU FABRICANTE: " + medicamento.getFabricante());
                                                        String fabricante = lector.nextLine();
                                                        if(fabricante != ""){
                                                            medicamento.setFabricante(fabricante);
                                                        }
                                                    }
                                                    else{
                                                        System.out.println("EL MEDICAMENTO NO EXISTE.");
                                                    }
                                                    break;
                                                    
                                                case 3:
                                                    //CONSULTAR MEDICAMENTOS.
                                                    
                                                    break;

                                                case 4:
                                                    //REGISTRAR CITAS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  REGISTRAR CITAS.   ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 5:
                                                    //CONSULTAR CITAS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  CONSULTAR CITAS.          ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 6:
                                                    //ACTUALIZAR CITAS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  ACTUALIZAR CITAS.         ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 7:
                                                    //REGISTRAR MEDICOS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  REGISTRAR MEDICOS.        ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 8:
                                                    //CONSULTAR MEDICOS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  CONSULTAR MEDICOS.        ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 9:
                                                    //ACTUALIZAR MEDICOS
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  ACTUALIZAR MEDICOS.       ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 10:
                                                    //REGISTRAR PACIENTES
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  REGISTRAR PACIENTES.      ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 11:
                                                    //CONSULTAR PACIENTES
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  CONSULTAR PACIENTES.      ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 12:
                                                    //ACTUALIZAR PACIENTES
                                                    System.out.println("\n\n\n");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    System.out.println("++++++++++++++  ACTUALIZAR PACIENTES.     ++++++++++++++");
                                                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                                    break;

                                                case 13:
                                                    System.out.println("SALIENDO DE MENÚ ADMINISTRADOR");
                                                    ma = false;
                                                    break;

                                                default: 
                                                    System.out.println("OPCIÓN QUE HA INGRESADO ES INCORRECTA");
                                                    break;
                                            }
                                        }
                                        else{
                                            System.out.println("LOS DATOS INGRESADOS NO PERTENECEN A NINGUN USUARIO.");
                                        }
                                    }
                                    break;

                                case 4:
                                    menuusuarios = false;                                    
                                    break;

                                case 5:
                                    menuusuarios = false;
                                    System.out.println("Gracias por utilizar nuestro sistema.");
                                    continuar = false;
                                    break;
                                    
                                default: 
                                    System.out.println("OPCIÓN QUE HA INGRESADO ES INCORRECTA");
                                    break;
                            }
                            break;
                        }
                    case 2:
                        //SOLO LOS PACIENTES PUEDEN REGISTRARSE.
                        break;

                    case 3:
                        System.out.println("Gracias por utilizar nuestro sistema.");
                        continuar = false;
                        break;
                        
                    default: 
                        System.out.println("OPCIÓN QUE HA INGRESADO ES INCORRECTA");
                        break;
                    
                }
            }
        }   
    } 
}
