/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.modelos;

import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author USUARIO
 */
public class Administrador extends Persona{
    
    public Administrador(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String tipoUsuario, String usuario, String contrasenia) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, tipoUsuario);
    }
    
    public Administrador(){}
    
    //CRUD guardar
    public void guardarAdministrador() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO administradores(cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,profesion,salario,tipoUsuario) +"
                + "VALUES('" + getCedula() + "','" + getNombre() + "','" + getApellido() + "','" + getEdad() + "','" + getTelefono() + "','" + 
                getCorreo() + "','" + getDireccion() + "','" + getUsuario() + "','" + getContrasenia() + "','" + getTipoUsuario() + "');";
        Conexion.ejecutarConsulta(sql);
    }
    
    //Consultar
    public boolean consultarAdministrador(String admin) throws ClassNotFoundException, SQLException{
        String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia, tipoUsuario FROM administradores"
                + " WHERE cedula='" + admin + "';";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while(rs.next()){
            this.setCedula(admin);
            this.setNombre(rs.getString("nombre"));
            this.setApellido(rs.getString("apellido"));
            this.setEdad(rs.getInt("edad"));
            this.setTelefono(rs.getString("telefono"));
            this.setCorreo(rs.getString("correo"));
            this.setDireccion(rs.getString("direccion"));
            this.setUsuario(rs.getString("usuario"));
            this.setContrasenia(rs.getString("contrasenia"));
            this.setTipoUsuario(rs.getString("tipousuario"));
        }
        if(admin != this.getCedula()){
            return false;
        }
        else{
            return true;
        }
    }
    
    //Actualizar
    public void actualizarAdministrador(String admin) throws ClassNotFoundException, SQLException{
        String sql = "UPDATE administradores SET cedula='" + getCedula() + "', nombre='" + getNombre() + "', apellido='" + getApellido() + "', edad='" + getEdad() + 
                "', telefono='" + getTelefono() + "', correo='" + getCorreo() + "', direccion='" + getDireccion() + "', usuario='" + getUsuario() +
                "', contrasenia='" + getContrasenia() + "', tipoUsuario='" + getTipoUsuario() + "' WHERE cedula = '" + admin + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    //Borrar
    public void borrarAdministrador(String admin) throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM administradores WHERE cedula='" + admin + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    @Override
    public String toString() {
        return "Administrador{" + "cedula=" + getCedula() + ", nombre=" + getNombre() + ", apellido=" + getApellido() +
                ", edad=" + getEdad() + ", telefono=" + getTelefono() + ", correo=" + getCorreo() + 
                ", direccion=" + getDireccion() + ", usuario=" + getUsuario() + ", contrasenia=" + getContrasenia() +
                ", tipoUsuario=" + getTipoUsuario() + '}';
        //return "Medico{" + "profesion=" + profesion + ", sueldo=" + sueldo + '}';
    }
    
}
