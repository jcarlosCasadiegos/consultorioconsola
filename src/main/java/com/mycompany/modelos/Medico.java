/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.modelos;

import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author USUARIO
 */
public class Medico extends Persona{
    
    //Atributos
    private String profesion;
    private double sueldo;

    public Medico(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String usuario, String contrasenia, String profesion, double sueldo, String tipoUsuario) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, tipoUsuario);
        this.profesion = profesion;
        this.sueldo = sueldo;
    }

    public Medico(){}

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    //CRUD guardar
    public void guardarMedico() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO medicos(cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,profesion,salario,tipoUsuario) +"
                + "VALUES('" + getCedula() + "','" + getNombre() + "','" + getApellido() + "','" + getEdad() + "','" + getTelefono() + "','" + 
                getCorreo() + "','" + getDireccion() + "','" + getUsuario() + "','" + getContrasenia() + "','" + getProfesion() + "','" +
                getSueldo() + "','" + getTipoUsuario() + "');";
        Conexion.ejecutarConsulta(sql);
    }
    
    //Consultar
    public boolean consultarMedico(String cedulam) throws ClassNotFoundException, SQLException{
        String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,profesion,salario,tipoUsuario FROM medicos"
                + "WHERE cedula='" + cedulam + "';";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while(rs.next()){
            this.setCedula(cedulam);
            this.setNombre(rs.getString("nombre"));
            this.setApellido(rs.getString("apellido"));
            this.setEdad(rs.getInt("edad"));
            this.setTelefono(rs.getString("telefono"));
            this.setCorreo(rs.getString("correo"));
            this.setDireccion(rs.getString("direccion"));
            this.setUsuario(rs.getString("usuario"));
            this.setContrasenia(rs.getString("contrasenia"));
            this.setProfesion(rs.getString("profesion"));
            this.setSueldo(rs.getDouble("salario"));
            this.setTipoUsuario(rs.getString("tipousuario"));
        }
        if(cedulam != this.getCedula()){
            return false;
        }
        else{
            return true;
        }
    }
    
    //Actualizar
    public void actualizarMedico(String cedulam) throws ClassNotFoundException, SQLException{
        String sql = "UPDATE medicos SET cedula='" + getCedula() + "', nombre='" + getNombre() + "', apellido='" + getApellido() + "', edad='" + getEdad() + 
                "', telefono='" + getTelefono() + "', correo='" + getCorreo() + "', direccion='" + getDireccion() + "', usuario='" + getUsuario() +
                "', contrasenia='" + getContrasenia() + "', profesion='" + getProfesion() + "', salario='" + getSueldo() + "', tipoUsuario='" + getTipoUsuario() + "' WHERE cedula = '" + cedulam + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    //Borrar
    public void borrarMedico(String medico) throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM medicos WHERE cedula='" + medico + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    @Override
    public String toString() {
        return "Medico{" + "cedula=" + getCedula() + ", nombre=" + getNombre() + ", apellido=" + getApellido() +
                ", edad=" + getEdad() + ", telefono=" + getTelefono() + ", correo=" + getCorreo() + 
                ", direccion=" + getDireccion() + ", usuario=" + getUsuario() + ", contrasenia=" + getContrasenia() +
                ", proesion=" + profesion + ", salario=" + sueldo + ", tipoUsuario=" + getTipoUsuario() + '}';
        //return "Medico{" + "profesion=" + profesion + ", sueldo=" + sueldo + '}';
    }
   
    
}
