/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.modelos;

import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author USUARIO
 */
public class Paciente extends Persona{

    public Paciente(String cedula, String nombre, String apellido, int edad, String telefono, String correo, String direccion, String usuario, String contrasenia, String tipoUsuario) {
        super(cedula, nombre, apellido, edad, telefono, correo, direccion, usuario, contrasenia, tipoUsuario);
    }
    
    public Paciente(){}
    
    //CRUD guardar
    public void guardarPaciente() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO pacientes(cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,tipoUsuario)"
                + "VALUES('" + getCedula() + "','" + getNombre() + "','" + getApellido() + "','" + getEdad() + "','" + getTelefono() + "','" + 
                getCorreo() + "','" + getDireccion() + "','" + getUsuario() + "','" + getContrasenia() + "','" + getTipoUsuario() + "');";
        Conexion.ejecutarConsulta(sql);
    }
    
    //Consultar
    public boolean consultarPaciente(String cedular) throws ClassNotFoundException, SQLException{
        String sql = "SELECT cedula,nombre,apellido,edad,telefono,correo,direccion,usuario,contrasenia,tipoUsuario FROM pacientes WHERE cedula='" + cedular + "';";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while(rs.next()){
            this.setCedula(cedular);
            this.setNombre(rs.getString("nombre"));
            this.setApellido(rs.getString("apellido"));
            this.setEdad(rs.getInt("edad"));
            this.setTelefono(rs.getString("telefono"));
            this.setCorreo(rs.getString("correo"));
            this.setDireccion(rs.getString("direccion"));
            this.setUsuario(rs.getString("usuario"));
            this.setContrasenia(rs.getString("contrasenia"));
            this.setTipoUsuario(rs.getString("tipoUsuario"));
        }
        if(cedular != this.getCedula()){
            return false;
        }
        else{
            return true;
        }
    }
    
    public boolean validarUsuario() throws ClassNotFoundException, SQLException{
        String sql = "SELECT usuario,contrasenia FROM pacientes WHERE usuario ='" + this.getUsuario() + "' AND contrasenia ='" + this.getContrasenia() + "';";
                
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        
        while(rs.next()){
            this.setUsuario(rs.getString("usuario"));
            this.setContrasenia(rs.getString("constrasenia"));
        }
        if(this.getUsuario() != null && this.getContrasenia() != null){
            return true;
        }
        else{               
            return false;
        }
    }
    
    
    //Actualizar
    public void actualizarPaciente(String cedulap) throws ClassNotFoundException, SQLException{
        String sql = "UPDATE pacientes SET cedula='" + getCedula() + "', nombre='" + this.getNombre() + "', apellido='" + getApellido() + "', edad='" + getEdad() + 
                "', telefono='" + getTelefono() + "', correo='" + getCorreo() + "', direccion='" + getDireccion() + "', usuario='" + getUsuario() +
                "', contrasenia='" + getContrasenia() + "', tipoUsuario='" + getTipoUsuario() + "' WHERE cedula ='" + cedulap + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    //Borrar
    public void borrarPaciente(String cedulap) throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM pacientes WHERE cedula=" + cedulap + "";
        Conexion.ejecutarConsulta(sql);
    }
    
       
    @Override
    public String toString() {
        return "Paciente{" + "cedula=" + getCedula() + ", nombre=" + getNombre() + ", apellido=" + getApellido() +
                ", edad=" + getEdad() + ", telefono=" + getTelefono() + ", correo=" + getCorreo() + 
                ", direccion=" + getDireccion() + ", usuario=" + getUsuario() + ", contrasenia=" + getContrasenia() +
                ", tipoUsuario=" + getTipoUsuario() + '}';
        //return "Medico{" + "profesion=" + profesion + ", sueldo=" + sueldo + '}';
    }
    
    
}
