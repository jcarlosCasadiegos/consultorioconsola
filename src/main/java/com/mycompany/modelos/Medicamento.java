/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.modelos;

import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author USUARIO
 */
public class Medicamento {
    
    private int id;
    private String nombre;
    private String presentacion;
    private String descripcion;
    private String fabricante;

    public Medicamento(int id, String nombre, String presentacion, String descripcion, String fabricante) {
        this.id = id;
        this.nombre = nombre;
        this.presentacion = presentacion;
        this.descripcion = descripcion;
        this.fabricante = fabricante;
    }
    
    public Medicamento(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }
    
    //Guardar
    public void guardarMedicamento() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO medicamentos(nombre,presentacion,descripcion,fabricante) "
                + " VALUES('" + getNombre() + "','" + getPresentacion() + "','" + getDescripcion() + "','" + getFabricante() + "');";  
        Conexion.ejecutarConsulta(sql);
    }
    
    public boolean consultarMedicamento(int id) throws ClassNotFoundException, SQLException{
        String sql = "SELECT id,nombre,presentacion,descripcion,fabricante FROM medicamentos"
                + " WHERE id='" + id + "'";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while(rs.next()){
            this.setId(id);
            this.setNombre(rs.getString("nombre"));
            this.setPresentacion(rs.getString("presentacion"));
            this.setDescripcion(rs.getString("descripcion"));
            this.setFabricante(rs.getString("fabricante"));
        }
        if(this.getId()!= id){
            return false;
        }
        else{
            return true;
        }
    }
    
    public void actualizarMedicamento(int id) throws ClassNotFoundException, SQLException{
        String sql = "UPDATE medicamentos SET nombre='" + getNombre() + "', presentacion='" + getPresentacion() + "', descripcion='" + getDescripcion() +
                "', fabricante='" + getFabricante() + "' WHERE id = '" + id + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    public void borrarMedicamento(int id) throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM medicamentos WHERE id='" + id + "';";
        Conexion.ejecutarConsulta(sql);
    }

    @Override
    public String toString() {
        return "Medicamento{" + "id=" + id + ", nombre=" + nombre + ", presentacion=" + presentacion + ", descripcion=" + descripcion + ", fabricante=" + fabricante + '}';
    }
    
    
}
