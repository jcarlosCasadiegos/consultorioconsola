/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.modelos;

import com.mycompany.persistencia.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class Cita {
    
    private int id;
    private String tipoCita;
    private String fechaRegistro;
    private String fechaCita;
    private String pacienteCedula;

    public Cita(int id, String tipoCita, String fechaRegistro, String fechaCita, String pacienteCedula) {
        this.id = id;
        this.tipoCita = tipoCita;
        this.fechaRegistro = fechaRegistro;
        this.fechaCita = fechaCita;
        this.pacienteCedula = pacienteCedula;
    }
    
    public Cita(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(String tipoCita) {
        this.tipoCita = tipoCita;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getPacienteCedula() {
        return pacienteCedula;
    }

    public void setPacienteCedula(String pacienteCedula) {
        this.pacienteCedula = pacienteCedula;
    }
    
    //Guardar
    public void guardarCita() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO citas(tipoCita,fechaRegistro,fechaCita,pacienteCedula)"
                + "VALUES('" + getTipoCita()+ "','" + getFechaRegistro()+ "','" + getFechaCita()+ "','" + getPacienteCedula() + "');";
        Conexion.ejecutarConsulta(sql);
    }
    
    public boolean consultarCita(String cedulaP) throws ClassNotFoundException, SQLException{

        String sql = "SELECT id,tipoCita,fechaRegistro,fechaCita,pacienteCedula FROM citas WHERE pacienteCedula='" + cedulaP + "';";

        ResultSet rs = Conexion.ejecutarConsulta(sql);

        while(rs.next()){
            this.setId(rs.getInt("id"));
            this.setTipoCita(rs.getString("tipoCita"));
            this.setFechaRegistro(rs.getString("fechaRegistro"));
            this.setFechaCita(rs.getString("fechaCita"));
            this.setPacienteCedula(cedulaP); 
        }        
        if (cedulaP != this.getPacienteCedula()) {
            return false;
        }
        else{
            return true;
        }
    }
    
    //Esta actualizacion hay que validarla para que solo la haga el administrador.
    
    public void actualizarCita(String id) throws ClassNotFoundException, SQLException{
        String sql = "UPDATE citas SET tipoCita='" + getTipoCita() + "', fechaCita='" + getFechaCita() + "' WHERE pacienteCedula = '" + id + "';";
        Conexion.ejecutarConsulta(sql);
    }
    
    public void borrarCita() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM citas WHERE id='" + getId() + "';";
        Conexion.ejecutarConsulta(sql);        
    }

    @Override
    public String toString() {
        return "Cita{" + "id=" + id + ", tipoCita=" + tipoCita + ", fechaRegistro=" + fechaRegistro + ", fechaCita=" + fechaCita + ", pacienteCedula=" + pacienteCedula + '}';
    }
    
    
}
